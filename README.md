# swduex
#### Star Wars API - proof of concept
---
It uses @vue/cli, vue3, vue-router, and a custom store  

---


##### Project setup
```
npm install
```

##### Compiles and hot-reloads for development
```
npm run serve
```

##### Compiles and minifies for production
```
npm run build
```

##### Lints and fixes files
```
npm run lint
```
