import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/:category',
    name: 'Category',
    component: () => import('../views/Main.vue')
  },
  {
    path: '/:category/:id',
    name: 'Item',
    component: () => import('../views/Record.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  // TODO: Change title per page
  // const title = to.meta.type.charAt(0).toUpperCase() + to.meta.type.slice(1)
  // document.title = `Swapi API Front-End | ${title}`
  next()
})

export default router
