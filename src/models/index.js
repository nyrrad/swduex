/* eslint-disable */

import axios from 'axios'
const api = 'https://swapi.dev/api/'

export const getCategory = async (cat, output = []) => {
  const response = await axios.get(api + cat)
  const { results, next } = response.data;
  output = [...output, ...results];
  if (next !== null) {
    return getCategory(next.slice(api.length), output)
  }
  return output;
}

export const prepNameTitleLink = (link, route) => {
  // const cleanedLink = link.url.replace(`https://swapi.dev/api${route}/`, '').replace('/', '')
  const cleaned = (cleanMe) => {
    return cleanMe.toLowerCase().replace(/\s/g, '_');
  }

  const cleanedLink = cleaned(link.name || link.title)
  return cleanedLink
}

/* eslint-disable */
