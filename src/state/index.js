/* eslint-disable */

import { reactive } from 'vue'
import { getCategory } from '../models'

// TODO: Needs Error Handling
export const store = ({
  state: reactive ({
    records: {}
  }),
  getters: {
    getRecords (category) {
      if (store.private.hasCategory(category)) {
        return store.state.records[category]
      }
    },
    getSingleRecord (category, item = null) {
      let output = {}
      if (store.private.hasCategory(category)) {
        let records = store.state.records[category]

        if (item) {
          return output = records.find((record, index) => {
            let recordName = (record.title || record.name).toLowerCase().replace(/\s/g, "_")
            if (recordName === item) return true
          })
        }
      }
    }
  },
  mutations: {
    async pushRecords (category, data) {
      if (store.private.hasCategory(category)) {
        return store.state.records[category]
      }

      store.state.records[category] = await getCategory(category)

      return store.state.records[category]
    }
  },
  private: {
    hasCategory (category) {
      return store.state.records.hasOwnProperty(category)
    }
  }
})

/* eslint-disable */
